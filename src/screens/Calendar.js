import React, { useEffect, useState } from 'react';
import { SelectedCalendar } from '../components/SelectedCalendar';

import '../theme/Calendar.css'
import { formaterFecha } from '../helpers/formatearFecha';

export const Calendar = ({ rangeStatic }) => {

    const [ rangeSelect, setRangeSelect ] = useState([]);
    const [ fecha1, setFecha1 ] = useState(null);
    const [ fecha2, setFecha2 ] = useState(null);
    const [startDate, setStartDate] = useState(null);
    const [click, isClick] = useState(false);
    const [endDate, setEndDate] = useState(null);
    const [ selectedRange, setSelectecRange ] = useState(null);
    const [ label, setLabel ] = useState('');

    const handleStaticRangeClick = ( range, label ) => {
        const { startDate, endDate } = range();

        setStartDate( startDate );
        setEndDate( endDate );
        setSelectecRange( true );
        setLabel(label)
        isClick(true);
    }


    useEffect(() => {
        if (rangeSelect?.length > 0) {
            const date1 = rangeSelect[0]?.start;
            const date2 = rangeSelect[0]?.end;

            const date1Enviar = ` ${ date1.getDate()}/${date1.getMonth() + 1}/${date1.getFullYear()}`;
            const date2Enviar = ` ${ date2.getDate()}/${date2.getMonth() + 1}/${date2.getFullYear()}`;

            setFecha1(formaterFecha(date1Enviar));
            setFecha2(formaterFecha(date2Enviar));
        }
        

    }, [rangeSelect])

    return (
        <div className='contenedor'>
            <div className='contenedor-column'>
                <ul className='rangos-estaticos'>
                    {
                        rangeStatic.map( (value, index) => (
                            <li 
                                key={ index }
                                onClick={ () => handleStaticRangeClick(value.range, value.label)}
                                className={ selectedRange === value.label ? 'selectedStaticRange' : '' }
                            >{ value.label }</li>
                        ))
                    }
                </ul>
            </div>
            <div style={{ width: "75%"}}>
                <div className='contenedor-header'>
                    <div className='contenedor-header-title'>
                        <div className='contenedor-header-title-1' 
                            style={{ 
                                // borderRight: "1px solid black",
                                width: "50%"
                            }}
                        >
                            <p className='contenedor-header-title-1-descripcion'>Desde:</p>
                        </div>
                        <div style={{
                            height: "50px",
                            borderRight: "1px solid black"
                        }}></div>
                        <div className='contenedor-header-title-1'>
                            <p className='contenedor-header-title-1-descripcion'>Hasta:</p>
                        </div>
                    </div>
                    
                </div>
                <div className='contenedor-body'>
                    <div className='contenedor-body-content'>
                        {/* <div className='contenedor-body-content-data diferente-tipo'> */}
                            
                            <SelectedCalendar 
                                rangeSelect={ rangeSelect } 
                                setRangeSelect = { setRangeSelect }
                                startDate = { startDate }
                                endDate = { endDate }
                                setStartDate = { setStartDate }
                                setEndDate = { setEndDate }
                                isClick = { isClick}
                                click = { click }
                                label= { label }
                            />
                            
                        {/* </div> */}
                    </div>  
                </div>
                <div className='contenedor-footer'>

                    <div className='contenedor-footer-info' style={{ width: "52%"}}>
                        {
                            // rangeSelect.length > 0 &&
                            <div style={{ display: "flex"}}>
                                <p className='contenedor-footer-info-fechas' style={{ marginLeft: "20px"}}>
                                    { fecha1 }
                                </p>
                                <label className='contenedor-footer-info-rango'>
                                    {"-->"}
                                </label>
                                <p className='contenedor-footer-info-fechas'>
                                    { fecha2 }
                                </p>
                            </div>
                        }
                        
                    </div>
                    {/* <div className='line-footer' style={{
                        borderRight: "1px solid black",
                        height: "63px"
                    }}>

                    </div> */}
                    <div className='contenedor-footer-acciones'>
                        <button className='contenedor-footer-acciones-button1'>Cancelar</button>
                        <button className='contenedor-footer-acciones-button1' style={{ marginLeft: "10px"}}>Aceptar</button>
                    </div>

                </div>
            </div>

        </div>
    )
}
