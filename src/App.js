import './App.css';
import { Calendar } from './screens/Calendar';
import { 
  addMonths,
  endOfMonth,
  endOfWeek,
  startOfMonth,
  startOfWeek,
  isSameDay,
} from "date-fns";

function App() {

  const defineds = {
    startUltimoTrismetre: startOfMonth(addMonths(new Date(), -3)),
    endUltimoTrimestre: endOfMonth(new Date()),
    startMesPasado: startOfMonth(addMonths(new Date(), -1)),
    endMesPasado: endOfMonth(addMonths(new Date(), -1)),
    startEsteMes: startOfMonth(startOfMonth(new Date())),
    endEsteMes: endOfMonth(new Date()),
    startOfWeek: startOfWeek(new Date()),
    endOfWeek: endOfWeek(new Date()),
  };
  
  const staticRangeHandler = {
    range: {},
    isSelected(range) {
      const defineRange = this.range();
      return (
        isSameDay(range.startDate, defineRange.startDate) &&
        isSameDay(range.endDate, defineRange.endDate)
      );
    },
  };
  
  const createStaticRanges = (ranges) => {
    return ranges.map((range) => ({ ...staticRangeHandler, ...range }));
  };


  const rangeStatic = createStaticRanges([
    {
      label: 'Ultimo trimestre',
      range: () => ({
        startDate: defineds.startUltimoTrismetre,
        endDate: defineds.endUltimoTrimestre
      })
    },
    {
      label: 'El mes pasado',
      range: () => ({
        startDate: defineds.startMesPasado,
        endDate: defineds.endMesPasado
      })
    },
    {
      label: 'Este mes',
      range: () => ({
        startDate: defineds.startEsteMes,
        endDate: defineds.endEsteMes
      })
    },
    {
      label: 'Esta semana',
      range: () => ({
        startDate: defineds.startOfWeek,
        endDate: defineds.endOfWeek
      })
    },
  ]);


  return (
    <div className="App"
      style={{
        display: "flex",
        justifyContent: "center",
        marginTop: '30px'
      }}
    >
      <Calendar rangeStatic={ rangeStatic } />
    </div>
  );
}

export default App;
