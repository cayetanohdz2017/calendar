
export const subtract = (mes, mesActual) => {

    const meses = [1,2,3,4,5,6,7,8,9,10,11,12];
    
    const currentIndex = meses.indexOf(mesActual);
    const targetIndex = meses.indexOf(mes);
    const monthsToGoBack = currentIndex - targetIndex + 1;
    if (mesActual < mes) {
        return Math.abs(monthsToGoBack)
    }
    
    return monthsToGoBack;
    

}