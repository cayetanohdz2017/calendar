

import { useEffect, useLayoutEffect, useRef, useState } from "react";
import moment from "moment";
import 'moment/locale/es';

import { subtract } from "../helpers/substract";
import { addYears } from "../helpers/addYears";
import { substractYears } from "../helpers/substractYears";

import "../theme/SelectCalendar.css";

export const SelectedCalendar = (props) => {

    const { 
        rangeSelect, 
        setRangeSelect,
        startDate,
        endDate,
        setStartDate,
        setEndDate,
        isClick,
        click,
        label
    } = props;

    const [ date, setDate ] = useState(new Date());
    const [ month, setMonth ] = useState(moment().month());
    const [ year, setYear ] = useState(moment().year());
    const [ year2, setYear2 ] = useState(moment().year());
    const [ allYear, setAllYear ] = useState([]);
    // const { month2, setMonth2 } = useState(moment(date).add(1, 'month').startOf('month'));
    
    // const month1 = useRef(moment(date).startOf('month'));
    // const month2 = useRef(moment(date).add(mont2Salto, 'month').startOf('month'));
    let month1 = null;
    let month2 = null;

    if (label === "Ultimo trimestre") {
        console.log(date);
        month1 = moment(date).startOf('month');
        month2 = moment(date).add(3, 'month').startOf('month');
    }else{
        month1 = moment(date).startOf('month');
        month2 = moment(date).add(1, 'month').startOf('month');
    }


    useEffect(() => {
        const dateNew = new Date(date)

        const monthToday = dateNew.getMonth();
        if (endDate) {
            setRangeSelect([{
                start: new Date(startDate),
                end: new Date(endDate)
            }])
        }

        if (startDate) {
            console.log(subtract(startDate?.getMonth() + 1, monthToday));
            const valorDinamico = subtract(startDate?.getMonth() + 1, monthToday)
            const prevMonth = moment(date).add(valorDinamico, 'month');
            console.log(prevMonth);
            setMonth( valorDinamico )
            // setDate(prevMonth);
        }
        

    }, [startDate, endDate]);




    const handleChange = (event, tipo, valor) => {
        console.log("hola");
        const numeroMes = parseInt(event.target.value) + 1;
        const dateNew = new Date(date)

        const monthToday = dateNew.getMonth();
        let valorDinamico = 0 ;

        if (tipo === 'month') {
            valorDinamico = subtract(numeroMes, monthToday)
        }

        switch ( tipo ) {
            case 'month':
                if (numeroMes === 12 && valor === 1 ) {
                    setYear2(year2 + 1);
                }

                if (numeroMes < 12 && valor === 1 ) {
                    setYear2( year2 - 1)
                }

                if (valor === 2 && numeroMes === 0) {
                    setYear( year - 1);
                    setMonth(parseInt(11));
                    break;
                }

                if (valor === 2 && numeroMes > 0) {
                    if (year === year2) {
                        setYear(year);
                    }else{
                        setYear(year + 1);
                    }
                    

                }

                const prevMonth = moment(date).add(valorDinamico, 'month');
                setMonth(parseInt(event.target.value))
                setDate(prevMonth);

                break;
            case 'year1':
                const currentYear = moment().year();

                let years = 0;
                if (parseInt(currentYear) < parseInt(event.target.value)) {
                    const saltosYear = addYears(currentYear, parseInt(event.target.value));
                    years = moment(date).add(saltosYear, 'year');
                }else{
                    const saltosYear = substractYears(currentYear, parseInt(event.target.value));
                    years = moment(date).subtract(saltosYear, 'year');
                }


                setYear(parseInt(event.target.value));
                setDate(years);
                break;
            case 'year2':
                setYear2(parseInt(event.target.value))
                break;

            default:
                break;
        }

    }


    const monthOptions = moment.months().map((label, value) => (
        <option key={value} value={value}>{label}</option>
    ));
    

    const monthOptionsPrueb = moment.months().map((label, value) => (
        <option key={value} value={value - 1}>{label}</option>
    ));


    useEffect(() => {
        const yearsBefore = [];
        const yearsAfter = [];
        for (let i = 0; i < 60; i++) {

            const yearAfter = year - i;
            yearsAfter.push(yearAfter);
            const yearBefore = year + i;
            yearsBefore.push(yearBefore)

        }
        const allYears = yearsAfter.concat(yearsBefore);
        const uniqueYears = allYears.filter((number, index) => {
            return allYears.indexOf(number) === index;
        });
        setAllYear(uniqueYears.sort())

    }, [])

    console.log(new Date(month1).getMonth());



    return (
        <div className="contenedor-calendar">
            <div className="contenedor-calendar-content" style={{ width: "51%"}}>
                <select key="monts" value={ month } onChange={ (event) => handleChange(event, 'month', 1) } className="selectMont" >
                    { monthOptions }
                </select>
                <select key="years" value={ year } onChange={ (event) => handleChange(event, 'year1') } className="selectMont nuevoEstilo" >
                    {
                        allYear.map((year, index) => (
                            <option key={ index } value={ year } >{ year }</option>
                        ))
                    }
                </select>
                <table style={{ marginTop: "10px"}}>
                    <thead>
                        <tr>
                            <th style={{ width: "30px"}}>L</th>
                            <th style={{ width: "30px"}}>M</th>
                            <th style={{ width: "30px"}}>M</th>
                            <th style={{ width: "30px"}}>J</th>
                            <th style={{ width: "30px"}}>V</th>
                            <th style={{ width: "30px"}}>S</th>
                            <th style={{ width: "30px"}}>D</th>
                        </tr>
                    </thead>
                    <tbody>
                        <RenderMonth 
                            month = { month1 } 
                            startDate = { startDate }
                            endDate = { endDate }
                            setStartDate = { setStartDate }
                            setEndDate = { setEndDate }  
                            isClick = { isClick }
                            click = { click }
                            setRangeSelect= { setRangeSelect }
                            label = { label }
                            
                        />
                    </tbody>
                </table>

            </div>
            <div className="line"></div>
            <div className="contenedor-calendar-content"
                style={{ 
                    paddingLeft: "14px"
                }}
            >
                <select key="monts" value={ label === "Ultimo trimestre" ? new Date(month1).getMonth() : month } onChange={ (event) => handleChange(event, 'month', 2) } className="selectMont" >
                    { monthOptionsPrueb }
                </select>
                <select key="years" value={ year2 } onChange={ (event) => handleChange(event, 'year2') } className="selectMont nuevoEstilo" >
                    {
                        allYear.map((year, index) => (
                            <option key={ index } value={ year } >{ year }</option>
                        ))
                    }
                </select>
                <table style={{ marginTop: "10px"}}>
                    <thead>
                        <tr>
                            <th style={{ width: "30px"}}>D</th>
                            <th style={{ width: "30px"}}>L</th>
                            <th style={{ width: "30px"}}>M</th>
                            <th style={{ width: "30px"}}>M</th>
                            <th style={{ width: "30px"}}>J</th>
                            <th style={{ width: "30px"}}>V</th>
                            <th style={{ width: "30px"}}>S</th>

                        </tr>
                    </thead>
                    <tbody>
                        <RenderMonth 
                            month={ month2 } 
                            startDate = { startDate }
                            endDate = { endDate }
                            setStartDate = { setStartDate }
                            setEndDate = { setEndDate }
                            isClick = { isClick }
                            click = { click }
                            setRangeSelect = { setRangeSelect }
                            label = { label }
                        />
                    </tbody>
                </table>
            </div>
        </div>
    )
}


const RenderMonth = (props) => {

    const { month, startDate, endDate, setStartDate, setEndDate, isClick, click, setRangeSelect, label } = props;

    const startDay = moment(month).startOf('month').day();
    const daysInMonth = moment(month).daysInMonth();
    let day = 1;
    let rows = [];
    let cells = [];
    


    const dayCurrent = moment().date(); // obtiene el dia actual
    const currentMonth = moment().month(); // Obtiene el mes actual
    const currentYear = moment().year(); // Obtiene el año actual
    const monthToRender = moment(month).month(); // Obtiene el mes que se está mostrando
    const yearToRender = moment(month).year(); // Obtiene el año que se está mostrando


    const handleClick = (days) => {
        if (!startDate) {
            setStartDate(days);
            setEndDate(null);
        } else if( days.isBefore(startDate) ){
            setStartDate(days);
            setEndDate(null);
        }else if ( days.isSame(startDate) ) {
            setStartDate(null);
            setEndDate(null);
        }else if( days.isAfter(endDate)){
            setStartDate(days);
            setEndDate(null);
        }else{
            setEndDate(days);
        }
        setRangeSelect(null);
        isClick(true);
    }


    for (let i = 0; i < 6; i++) {
        for (let j = 0; j < 7; j++) {
        if (i === 0 && j < startDay) { // Agrega en donde comienza cada fecha segun el dia
            cells.push(<td key={`${i}-${j}`} />);
        } else if (day > daysInMonth) {
            break;
        } else {
            const date = moment(`${day}-${month.format('M')}-${month.format('YYYY')}`, 'D-M-YYYY');

            let className = null;
            // console.log(startDate && date.isSame(startDate, 'day'));
            // console.log(endDate && date.isBetween(startDate, endDate, 'day', '[]'));
            if(click){
                if (label === "El mes pasado") {
                    className = 
                        (startDate && date.clone().subtract(1, 'month').isSame(startDate, 'month'))
                            ? 'selectedRange'
                            : '';
                }else{
                    className =
                        (startDate && date.isSame(startDate, 'day'))
                            ? 'selectedRange'
                            : (endDate && date.isBetween(startDate, endDate, 'day', '[]'))
                                ? 'selectedRange'
                                : '';
                }
                
                

            }else{
                className = 
                    (day === dayCurrent && currentMonth === monthToRender && currentYear && yearToRender) ? 'todayDay' : '';
            }
            
            cells.push(<td key={`${i}-${j}`} className={ className } onClick={ () => handleClick(date) } >{day}</td>);
            day++;
        }
        }
        rows.push(<tr key={i}>{cells}</tr>);
        cells = [];
    }

    return rows;
}